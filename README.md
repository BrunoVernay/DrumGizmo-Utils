# DrumGizmo-Utils

Stuff to use with [DrumGizmo](https://drumgizmo.org/). Ardour templates, Hydrogen mappings ...


Forum where most of the stuff came: https://linuxmusicians.com/viewforum.php?f=55

## DrumGizmo

In itself, it does not do much: No sequencer, no sounds ... But it will load a sound bank (up to a few Go ...) and play them with velocity and timing humanization, distribute the sounds over multiple channels ...

DrumGizmo uses its own proprietary SoundFont format. No `sf2`, `sf3` or `sfz` and no way to use [the 45 freely available unpitched percussions Soundfonts](https://www.polyphone-soundfonts.com/en/soundfonts/percussion/unpitched-percussion) you get only about 5. There is a SoundFont editor in early beta: [DGEdit](https://drumgizmo.org/wiki/doku.php?id=dgedit:roadmap)

It comes as 2 shapes:
- LV2 plugin instrument.
- Command Line tool, either to play a MIDI file or to be used as a *Jack MIDI* client.



## Hydrogen mappings

Source: [Using Hydrogen to play DrumGizmo's DRS Kit](https://linuxmusicians.com/viewtopic.php?f=48&t=15946)

[Hydrogen (AKA "H2")](http://hydrogen-music.org/) is a Drum Machine. Development slowed in 2011, but re-started in 2018. https://github.com/hydrogen-music/hydrogen 

The strong point about H2 is its interface: quite handy. We will use it, but just to send Midi.

Then, there are 2 ways to use it with DrumGizmo.
- DrumGizmo started via the command line as a Jack midi client.
- DrumGizmo started a LV2 plugin in [Ardour](https://ardour.org/) (or other DAW)
 
TBC ...

## Ardour templates

Source: [Using Hydrogen to play DrumGizmo's DRS Kit](https://linuxmusicians.com/viewtopic.php?f=48&t=15946) (Same source yes.)

[Ardour](https://ardour.org/) can use [DrumGizmo](https://drumgizmo.org/)'s LV2 plugin on a MIDI track.
DrumGizmo will provides 16 output channels! To avoid manually create all these, there are templates that provides the whole configuration already.

Ardour's Templates are in:
- `~/.config/ardour5/templates/` Users
- `/usr/share/ardour5/templates/` Factory
- They can be imported from within Ardour `Windows/Templates` then `Import`

Looks like there are 2 kinds of templates:
- [Session Templates](http://manual.ardour.org/working-with-sessions/session-templates/)
- [Track Templates](http://manual.ardour.org/working-with-tracks/adding-tracks-and-busses/)

Currently the templates (I copied) are "Session Template", but:
- *Session Templates* can only be used for new sessions and not easily combined
- LUA scripts could create the 16 channels and more.

### Track Templates

One can save *Track Templates* but they will be restored as just track. It will not be as simple as saving a bunch of tracks and restore them as is.

It may require more work but seems to be the *definitive solution*:
- Allows to start with any template and add a "DrumGizmo track" via the [Add Track/Bus/VCA](http://manual.ardour.org/working-with-tracks/adding-tracks-and-busses/)
- This is how a "live Band" is added, with lots of choices
- It allows to do LUA scripting.

### Perfect example

This Video [Introducing the AVL Drumkits LV2 Plugin!](https://youtu.be/4idMZTxTaY8?t=9m15s) looks like the perfect solution:
- can fan out to Track or Busses
  - Tracks (or Busses) are nicely grouped
  - They can be sent to a sub-bus (see at 12'20)
- The notes names appear on the piano-roll ([MIDNAM](http://manual.ardour.org/working-with-midi/patch-change/))
- http://x42-plugins.com/x42/x42-avldrums 

Looking at the repo [Robin Gareus - avldrums.lv2 - AVLinux Drumkits](https://github.com/x42/avldrums.lv2) it may not be easy. I can see no LUA, only C.

**TODO**
- Try to understand how it is done in AVLDrums plugin (_seems difficult!_)
- Or try to understand "Track Templates" and [LUA scripting in Ardour](http://manual.ardour.org/lua-scripting/) (_seems easier_)

LUA examples:
- https://github.com/Ardour/ardour/blob/master/scripts/create_drum_tracks.lua
- https://github.com/Ardour/ardour/blob/master/scripts/_fan_out_instrument.lua
- "Live Band" *Track template* https://github.com/Ardour/ardour/blob/master/scripts/template_band.lua

